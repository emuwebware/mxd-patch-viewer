import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MxdInterfaceComponent } from './mxd-interface.component';

describe('MxdInterfaceComponent', () => {
  let component: MxdInterfaceComponent;
  let fixture: ComponentFixture<MxdInterfaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MxdInterfaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MxdInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
